package finitefield

// #include "ff_2_256.h"
import "C"

import (
  "bytes"
  "encoding/binary"
//  "log"
)

// 256 bits fit in 32 bytes
const BLOCKS_PER_FIELD_ELEMENT int = 8
const BYTES_PER_BLOCK int = 4
type ElementBytes [BLOCKS_PER_FIELD_ELEMENT*BYTES_PER_BLOCK]byte

type FieldElement struct {
  elm C.ff_element
}

func Zero() FieldElement {
  var f FieldElement
  f.elm = C.ff_zero
  return f
}

func One() FieldElement {
  var f FieldElement
  f.elm = C.ff_one
  return f
}

func Add(a, b FieldElement) FieldElement {
  var c FieldElement
  C.ff_add(&a.elm[0], &b.elm[0], &c.elm[0])
  return c
}

func Square(a FieldElement) FieldElement {
  var r FieldElement
  C.ff_square(&a.elm[0], &r.elm[0])
  return r
}

func Invert(a FieldElement) FieldElement {
  var r FieldElement
  C.ff_inv(&a.elm[0], &r.elm[0])
  return r
}

func Mul(a, b FieldElement) FieldElement {
  var c FieldElement
  C.ff_mul_lrcomb_w4(&a.elm[0], &b.elm[0], &c.elm[0])
  return c
}

func Div(a, b FieldElement) FieldElement {
  var c FieldElement
  C.ff_div(&a.elm[0], &b.elm[0], &c.elm[0])
  return c
}

func Equal(a, b FieldElement) bool {
  return C.ff_eq(&a.elm[0], &b.elm[0]) > 0
}

// WARNING: This code does not check if the user
// sets an integer larger than 
func Set(b ElementBytes) FieldElement {
  var dat [BLOCKS_PER_FIELD_ELEMENT]C.ff_block
  var ret FieldElement
  for i := range dat {
    start := i*BYTES_PER_BLOCK
    buf := bytes.NewBuffer(b[start:(start+BYTES_PER_BLOCK)])
    binary.Read(buf, binary.BigEndian, &dat[i])
  }
  C.ff_set(&ret.elm[0], &dat[0])
  return ret
}

func Get(f FieldElement) ElementBytes {
  var ret ElementBytes
  var cint C.ff_block
  for i := 0; i<BLOCKS_PER_FIELD_ELEMENT; i++ {
    cint = (&f.elm[0]).data[i]
    start := i * BYTES_PER_BLOCK
    buf := new(bytes.Buffer)
    binary.Write(buf, binary.BigEndian, uint32(cint))
    copy(ret[start:(start+BYTES_PER_BLOCK)], buf.Bytes())
  }
  return ret
}

