package finitefield

import (
//  "log"
  "testing"
)

func TestAdd(t *testing.T) {
  a := One()
  b := One()
  Add(a, b)
}

func TestSetZero(t *testing.T) {
  a := Zero()

  var bytes ElementBytes
  b := Set(bytes)

  if !Equal(a,b) {
    t.Fail()
  }
}

func TestSetOne(t *testing.T) {
  a := One()

  var bytes ElementBytes
  bytes[3] = 0x01
  b := Set(bytes)

  if !Equal(a,b) {
    t.Fail()
  }

}

func TestSetGet(t *testing.T) {
  var bytes ElementBytes
  bytes[2] = 0x23
  bytes[3] = 0x21
  bytes[13] = 0x21
  //log.Printf("%v", bytes)
  b1 := Set(bytes)
  s := Get(b1)
  //log.Printf("%v", s)
  b2 := Set(s)

  if !Equal(b1, b2) {
    t.Fail()
  }
}

func TestSetGet2(t *testing.T) {
  var bytes ElementBytes
  bytes[3] = 0x03
  b1 := Set(bytes)
  bytes[3] = 0x05
  b2 := Set(bytes)

  // (x+1)^2 == (x^2 + 1) in F_{2^256}
  if !Equal(Mul(b1, b1), b2) {
    t.Fail()
  }
}

